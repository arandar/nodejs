const currentDate = new Date();
module.exports.date = currentDate;
const day = currentDate.getDate();
const month = currentDate.getMonth();
const year = currentDate.getFullYear();
module.exports.fullDate = `0${day}.${month+1}.${year}`;