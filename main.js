const express = require('express');
const parser = require('body-parser');
const fs = require('fs');
const os = require('os');
const date = require('./date');
const app = express();
const mongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017/';

const urlencodedParser = parser.urlencoded({extended: false});

app.use(express.static(__dirname + '/public'));

//вывод данных на экран и запись в файл
app.post('/result', urlencodedParser, function(request, response) {
	try {
		console.log(request.body);
		// const user = { name: request.body.userName, age: request.body.userAge };
		// response.send(user);
		const userName = request.body.userName;
		const userAge = request.body.userAge;
		const user = {  name: userName, age: userAge };
		
		fs.appendFile('public/assets/test.doc', `\n${request.body.userName} - ${request.body.userAge}`, function(error){
			if(error) throw error;
		});
	
		// запись в БД
		mongoClient.connect(url, (error, client) => {
			const database = client.db('usersdb');
			const collection = database.collection('users');
	
			collection.insertOne(user, (error, result) => {
				if(error) return response.status(400).send();
	
				response.send(user);
				client.close();
			});
		});
	} catch(error) {
		console.log(error);
		response.sendStatus(400).send();
	}
});

app.post('/deleted', urlencodedParser, function(request, response) {
	try {
		console.log(request.body);
		// const user = { name: request.body.userName, age: request.body.userAge };
		// response.send(user);
		const userName = { name: request.body.userName };
		
		fs.appendFile('public/assets/test.doc', `\n${request.body.userName} - ${request.body.userAge}`, function(error){
			if(error) throw error;
		});
	
		// deleting from DB
		mongoClient.connect(url, (error, client) => {
			const database = client.db('usersdb');
			const collection = database.collection('users');
	
			collection.findOneAndDelete(userName, (error, result) => {
				console.log(result);
				response.send(userName);
				client.close();
			});
		});
	} catch(error) {
		console.log(error);
		response.sendStatus(400).send();
	}
});

// чтение и вывод html
app.get('/', (request, response) => {
    // вывод json на экран
    // const content = fs.readFileSync('public/assets/text.json', 'utf8');
    // const users = JSON.parse(content);
	// response.send(users);
	try{
		fs.readFile('public/assets/test.html', function(error, data){
                 
			if(error) throw error;
			response.end(data);
		})
	} catch(error) {
		console.log(error);
		throw error;
	}
});

app.get('/delete', (request, response) => {
	try {
		fs.readFile('public/assets/delete.html', (error, data) => {
			if(error) throw error;
			response.end(data);
		});
	} catch(error) {
		console.log(error);
		throw error;
	}
});

// вывод из БД
app.get('/api/usersdb', (request, response) => {
	mongoClient.connect(url, (error, client) => {
		const db = client.db('usersdb');
		const collection = db.collection('users');

		if(error) throw error;
		collection.find().toArray((error, results) => {
			response.send(results);
			client.close();
		})
	})
})

// чтение json
app.get('/api/users', (request, response) => {
	fs.readFile('public/assets/text.json', 'utf8', (error, data) => {
		if(error) throw error;
		const users = JSON.parse(data);
		response.send(users);
	})
});

// выбор json по id
app.get('/api/users/:id', (request, response) => {
	const id = request.params.id;
	fs.readFile('public/assets/text.json', 'utf8', (error, data) => {
		if(error) throw error;
		const users = JSON.parse(data);
		const result = users.filter(user => user.id == id);
		if(result) {
			response.send(result);
		} else {
			response.status(404).send();
		}
		
	})
});

// запись в text.doc
app.get('/api/request', function(request, response){
	const userName = os.userInfo().username;
	const currentDate = new Date();
	fs.writeFile('public/assets/text.doc', `Привет, ${userName}! Сегодня ${date.fullDate} \n${currentDate}`, function(error){
                 
		if(error) throw error;
		response.end();
		return;
	})
});
app.listen(3000);